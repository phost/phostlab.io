---
date: 2024-03-08T00:00:00+09:00
---

# PIRI Apps on Play Privacy Policy

## 1 Definitions and Scope

- "PIRI Apps on Play" refers to [software published on Google Play Store by PIRI](https://play.google.com/store/apps/dev?id=8479485077301187435).
- This Privacy Policy only applies to PIRI Apps on Play.
- This Privacy Policy does not apply to [this website](https://phost.gitlab.io) or any other software published by PIRI.

## 2 Handling of Information

- PIRI does not collect any personally identifiable information about you when you use PIRI Apps on Play.
- Some PIRI Apps on Play may include peer-to-peer networking capabilities, for e.g. multiplayer features. If you choose to use such features, technical information about your device, including but not limited to IP addresses, may be transmitted to peer devices. PIRI is not involved in this process, and does not receive or otherwise have access to any of the information exchanged.

## 3 Other

- This Privacy Policy is not a contract.

## 4 Additional Information

- Source code for PIRI Apps on Play, and other PIRI software, can be found [here](https://gitlab.com/phost).
