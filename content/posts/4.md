---
title: "Using PIRI Life-likes to make emblems"
date: 2020-02-10T20:00:00+09:00
draft: false
tags:
- Android
- Art
---

How would you make emblems like this?

![Emblem](emblem512.png "Emblem")

First, download and install [PIRI Life-likes](https://gitlab.com/phost/piri-lifelikes/-/tags) or your Life simulator of choice. Then play around with the rules or search the internet until you find one that gives you interesting patterns. Some ideas:

* b234/s23467
* b0/s8
* b1/s
* b4/s2345
* b0234/s8
* b015678/s8
* b368/s34567

When you have a good pattern, screenshot it(or export as image, if the simulator supports it):

![Original pattern](emblemoriginal.png "Original pattern")

Then apply tiny planet filter to it. You can use [nomacs](https://nomacs.org/), a FOSS image viewer, for this(Adjustments-Tiny Planet):

![Tiny planet](tinyplanet.png "Tiny planet")

After saving the result as another file, it's only a matter of sharpening and cropping. I recommend sharpening in [GIMP](https://www.gimp.org)(Filters-Enhance-Sharpen) as nomacs sharpening doesn't seem to work.

![Result](emblem512.png "Result")
