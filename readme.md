# phost.gitlab.io
My personal blog.

## Building
Run `./build`

## Theme
Modified [Ananke](https://themes.gohugo.io/gohugo-theme-ananke/) under [MIT License](https://github.com/budparr/gohugo-theme-ananke/blob/master/LICENSE.md)
